import {timViTri} from "./controller/controller.js"
// fake dữ liệu database
let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];

// funtiom render danh sách kính từ dữ liệu
const renderGlassesList = () => {
    const contentHTML = dataGlasses.map(item => {
        return `<img id="glass-item" class="col-4" src="${item.src}" data-id="${item.id}" alt="Lỗi"></img>`
    })
    document.getElementById("vglassesList").innerHTML = contentHTML.join('')
}

const handleEvents = () => {
    const beforeGlass = document.getElementById("beforeGlass")
    const afterGlass = document.getElementById("afterGlass")
    const btnGlasslist = document.querySelectorAll("#glass-item")

    btnGlasslist.forEach((item) => {
        // khi click vào kính thì render kính ra và render thông tin của kính
        item.addEventListener("click", function(e) {
            // lấy ra được vị trí của từng item
            const viTri = timViTri(dataGlasses, item)
            if(viTri != -1) {
                document.getElementById("avatar").innerHTML = `<img src="${dataGlasses[viTri].virtualImg}" alt="Lỗi" />`
                
                // function render thông tin của kính
                renderGlassInfo(viTri)
            }else {
                return
            }


            // Khi click vào before sẽ mất kính đi
            beforeGlass.onclick = function() {
                // function remove kính
                removeGlasses()
            }
            // Khi click vào after show kính 
            afterGlass.onclick = function() {
                // function add kính
                addGlasses(viTri)
            }
        })

    })
    function removeGlasses() {
        document.getElementById("avatar").innerHTML = ""
        glassesInfo.style.display = "none"
    }
    function addGlasses(viTri) {
        document.getElementById("avatar").innerHTML = `<img src="${dataGlasses[viTri].virtualImg}" alt="Lỗi" />`
        glassesInfo.style.display = "block"
    }

}



const renderGlassInfo = (viTri) => {
    const glassesInfo = document.getElementById("glassesInfo")
    glassesInfo.style.display = "block"
    glassesInfo.innerHTML = `<div id="glasses">
        <h4 class="d-inline">${dataGlasses[viTri].name} - </h4> 
        <h4 class="d-inline">${dataGlasses[viTri].brand}</h4>
        <h5 class="my-3">${"Color: " + dataGlasses[viTri].color}</h5>
        <span class="bg-danger p-1">${"$"+dataGlasses[viTri].price}</span>
        <p class="mt-3">${dataGlasses[viTri].description}</p>
    </div>`
}


// hàm chạy global
function start() {
    renderGlassesList()
    handleEvents()
}

start()